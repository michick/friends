<?php

class ChainFinder
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * ChainFinder constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    /**
     * @param int|string $ida
     * @param int|string $idb
     * @return string[]
     */
    public function find($ida, $idb)
    {
        $ida = strval(intval($ida));
        $idb = strval(intval($idb));

        if ($ida == $idb) {
            return [$ida];
        }

        $friends = $this->getLevelOneTwoFriends([$ida, $idb]);

        return $this->bfs($friends, $ida, $idb);
    }

    /**
     * @param array $ids
     * @return array
     * @throws Exception
     */
    protected function getLevelOneTwoFriends(array $ids)
    {
        $idsString = implode(',', $ids);

        $dbh = new PDO($this->config['dsn'], $this->config['username'], $this->config['password']);
        $table = $this->config['table'];

        $stmt = $dbh->prepare("
SELECT DISTINCT user_id, friend_id FROM $table
WHERE user_id IN ($idsString) OR friend_id IN ($idsString)
");

        if (!$stmt->execute()) {
            $errorInfo = $stmt->errorInfo();
            $message = implode(' | ', $errorInfo);
            throw new Exception($message);
        }

        $friendsLevelOne = [];
        foreach ($stmt as $row) {
            $friendsLevelOne[] = $row['user_id'];
            $friendsLevelOne[] = $row['friend_id'];
        }

        $friendsLevelOne = array_unique($friendsLevelOne);

        if (empty($friendsLevelOne)) {
            return [];
        }

        $idsString = implode(',', $friendsLevelOne);
        $stmt = $dbh->prepare("
SELECT DISTINCT user_id, friend_id FROM $table
WHERE user_id IN ($idsString) OR friend_id IN ($idsString)
");

        if (!$stmt->execute()) {
            $errorInfo = $stmt->errorInfo();
            $message = implode(' | ', $errorInfo);
            throw new Exception($message);
        }

        $friendsGrouped = [];

        foreach ($stmt as $row) {
            $friendsGrouped[strval($row['user_id'])][] = strval($row['friend_id']);
            $friendsGrouped[strval($row['friend_id'])][] = strval($row['user_id']);
        }

        foreach ($friendsGrouped as $id => $friends) {
            $friendsGrouped[$id] = array_unique($friends);
        }

        return $friendsGrouped;
    }

    /**
     * @param array $friends
     * @param $a
     * @param $b
     * @return array
     */
    public function bfs(array $friends, $a, $b)
    {
        $a = strval($a);
        $b = strval($b);

        if (!array_key_exists($a, $friends)) {
            return [];
        }

        if ($a == $b) {
            return [$a];
        }

        $visited = [];
        $queue = [];
        $parents = [];
        $found = false;

        $queue[] = $friends[$a];

        foreach ($friends[$a] as $friend) {
            $parents[$friend] = $a;
        }

        $visited[] = $a;

        while ($queue) {
            $currents = array_shift($queue);
            if (in_array($b, $currents)) {
                $found = true;
                break;
            }

            foreach ($currents as $current) {
                if (in_array($current, $visited)) {
                    continue;
                }

                $visited[] = $current;
                array_push($queue, $friends[$current]);

                foreach ($friends[$current] as $friend) {
                    if (!isset($parents[$friend]) && !in_array($friend, $visited) && !in_array($friend, $currents)) {
                        $parents[$friend] = $current;
                    }
                }
            }
        }

        if (!$found) {
            return [];
        }

        $path = [];
        $startBack = $b;

        while (!empty($parents[$startBack]) && $startBack != $a) {
            $path[] = $startBack;
            $startBack = $parents[$startBack];
        }

        $path[] = $a;

        return array_reverse($path);
    }
}
