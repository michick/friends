<?php

require_once 'ChainFinder.php';

class ChainFinder2 extends ChainFinder
{
    /**
     * @param array $ids
     * @return array
     * @throws Exception
     */
    protected function getLevelOneTwoFriends(array $ids)
    {
        $idsString = implode(',', $ids);

        $dbh = new PDO($this->config['dsn'], $this->config['username'], $this->config['password']);
        $table = $this->config['table'];

        $stmt = $dbh->prepare("
SELECT * FROM $table f 
JOIN $table ff ON (f.user_id=ff.user_id OR f.user_id=ff.friend_id OR f.friend_id=ff.user_id OR f.friend_id=ff.friend_id)
WHERE f.user_id IN ($idsString) OR f.friend_id IN ($idsString)
");

        if (!$stmt->execute()) {
            $errorInfo = $stmt->errorInfo();
            $message = implode(' | ', $errorInfo);
            throw new Exception($message);
        }

        $friendsGrouped = [];

        foreach ($stmt as $row) {
            $friendsGrouped[strval($row['user_id'])][] = strval($row['friend_id']);
            $friendsGrouped[strval($row['friend_id'])][] = strval($row['user_id']);
        }

        foreach ($friendsGrouped as $id => $friends) {
            $friendsGrouped[$id] = array_unique($friends);
        }

        return $friendsGrouped;
    }
}
