<?php

/**
 * Class ChainFinderPureJoins
 * @warning Works only if table has for each connection two records a->b and b->a
 */
class ChainFinderPureJoins
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * ChainFinderPureJoins constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    /**
     * @param int|string $ida
     * @param int|string $idb
     * @return int[]
     */
    public function find($ida, $idb)
    {
        $ida = strval(intval($ida));
        $idb = strval(intval($idb));

        if ($ida == $idb) {
            return [$ida];
        }

        $dbh = new PDO($this->config['dsn'], $this->config['username'], $this->config['password']);
        $table = $this->config['table'];

        $res = $dbh->query("
            SELECT f.user_id as u1, f.friend_id as f1 
              FROM $table f 
              WHERE f.user_id=$ida AND f.friend_id=$idb
              LIMIT 1
        ", PDO::FETCH_ASSOC);
        if ($res && $records = $res->fetchAll()) {
            return $records[0];
        }

        $res = $dbh->query("
            SELECT f.user_id as u1,f.friend_id as f1, ff.friend_id as f2 
              FROM $table f 
              JOIN $table ff ON f.friend_id=ff.user_id
              WHERE f.user_id=$ida AND ff.friend_id=$idb
              LIMIT 1
        ", PDO::FETCH_ASSOC);
        if ($res && $records = $res->fetchAll()) {
            return $records[0];
        }


        $res = $dbh->query("
            SELECT f.user_id as u1, f.friend_id as f1, ff.friend_id as f2, fff.friend_id as f3
              FROM $table f 
              JOIN $table ff ON f.friend_id=ff.user_id
              JOIN $table fff ON ff.friend_id=fff.user_id
              WHERE f.user_id=$ida AND fff.friend_id=$idb
              LIMIT 1
        ", PDO::FETCH_ASSOC);
        if ($res && $records = $res->fetchAll()) {
            return $records[0];
        }

        $res = $dbh->query("
            SELECT f.user_id as u1, f.friend_id as f1, ff.friend_id as f2, fff.friend_id as f3, ffff.friend_id
              FROM $table f 
              JOIN $table ff ON f.friend_id=ff.user_id
              JOIN $table fff ON ff.friend_id=fff.user_id
              JOIN $table ffff ON fff.friend_id=ffff.user_id
              WHERE f.user_id=$ida AND ffff.friend_id=$idb
              LIMIT 1
        ", PDO::FETCH_ASSOC);
        if ($res && $records = $res->fetchAll()) {
            return $records[0];
        }

        return [];
    }
}
