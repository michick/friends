Task:
-----
Implement method finding a shortest path between 2 members via their friends (no longer than 5 laces in a chain in total, including users) - i.e. MySQL queries, algorithm, etc. Output should be array of facebook ids.

Short explanation:
------------------
Since chain should have no more than 5 friends, for two given IDs it makes sense to select from the database only their friends of level one and two. If there is a chain between given IDs (no longer than 5 friends) it will be in the result of the selection.

Usage:
------
**Warning: main method from ChainFinderPureJoins requires in the table of user connections for each connection to have two records
link a->b and link b->a.**

Which should be the case if you import friends of a user when one registers in the system.

I created test table with 2.25 million connections (more than 30K users) on my laptop.

ChainFinderPureJoins on any two incoming IDs what I tested gave result in less than 2ms.

1. Change DB access parameters in the config.php
2. Check if two users are connected within 4 handshakes:

`php run.php 3 8`

3. To use other finders specify 1 or 2 as the third argument, like:

`php run.php 3 8 1`

Load fixtures:
--------------
**Warning: this will truncate the table from config.php if exists**

1. Change db access parameters in the top of the file load_fixtures.php
2. Run

`php load_fixtures.php`

Tests:
------
1. To test BFS with in-memory data run

`php test.php`

2. To test chain search on db, load fixtures first, then

`php test_db.php`
