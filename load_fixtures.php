<?php

$config = require 'config.php';

$dbh = new PDO($config['dsn'], $config['username'], $config['password']);
$table = $config['table'];

$dbh->query("
CREATE TABLE IF NOT EXISTS `$table` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `friend_id` bigint(20) DEFAULT NULL,
  `friend_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `friend_id` (`friend_id`),
  KEY `user_friend` (`user_id`,`friend_id`)
) ENGINE=InnoDB AUTO_INCREMENT=80641924 DEFAULT CHARSET=utf8;

TRUNCATE `$table`;

INSERT INTO `$table` (`id`, `user_id`, `friend_id`, `friend_name`) VALUES
(1, 1, 3, NULL),
(2, 1, 8, NULL),
(3, 3, 2, NULL),
(4, 11, 7, NULL),
(5, 7, 2, NULL),
(6, 7, 9, NULL),
(7, 3, 6, NULL),
(8, 7, 3, NULL),
(9, 11, 15, NULL),
(10, 16, 11, NULL),
(11, 8, 11, NULL),
(12, 12, 9, NULL),
(13, 13, 18, NULL),
(14, 20, 19, NULL),
(15, 6, 18, NULL),
(16, 5, 10, NULL),
(17, 14, 18, NULL),
(18, 10, 20, NULL),
(19, 6, 4, NULL),
(20, 9, 17, NULL),
(21, 1, 3, NULL),
(22, 13, 9, NULL),
(23, 4, 5, NULL),
(24, 21, 7, NULL),
(25, 21, 9, NULL),
(26, 21, 6, NULL),
(27, 3, 1, NULL),
(28, 1, 3, NULL);
");
