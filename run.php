<?php

require_once 'ChainFinder.php';
require_once 'ChainFinder2.php';
require_once 'ChainFinderPureJoins.php';

$config = require 'config.php';

if (empty($argv[1]) || empty($argv[2])) {
    throw new Exception('Two user IDs should be specified. E.g.: php run.php 5 10');
}

$finderType = !empty($argv[3]) ? intval($argv[3]) : 0;

$a = intval($argv[1]);
$b = intval($argv[2]);

switch ($finderType) {
    case 1:
        $finder = new ChainFinder($config);
        break;
    case 2:
        $finder = new ChainFinder2($config);
        break;
    default:
        $finder = new ChainFinderPureJoins($config);
}

$start = microtime(true);

$path = $finder->find($a, $b);

echo get_class($finder) . ' execution time: ' . round((microtime(true) - $start) * 1000, 2) . ' ms' . PHP_EOL;

if (empty($path)) {
    echo 'Not connected within four handshakes.';
} else {
    echo implode(' -> ', $path);
}

echo PHP_EOL;
