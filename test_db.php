<?php

require_once 'ChainFinder.php';
$config = require 'config.php';

$testSet = [
    ['1', '7', ['1', '3', '7']],
    ['3', '9', ['3', '7', '9']],
    ['9', '3', ['9', '7', '3']],
    ['2', '9', ['2', '7', '9']],
    ['19', '4', ['19', '20', '10', '5', '4']],
    ['12', '6', ['12', '9', '21', '6']],
    ['1', '1', ['1']],
    ['15', '16', ['15', '11', '16']],
    ['13', '3', ['13', '9', '7', '3']],
    ['19', '6', []],
    ['8', '3', ['8', '1', '3']],
    ['8', '7', ['8', '11', '7']],
    ['12', '6', ['12', '9', '21', '6']],
    ['21', '9', ['21', '9']],
    ['2', '7', ['2', '7']],
    ['111', '111', ['111']],
];

$finder = new ChainFinder($config);

$errors = [];

foreach ($testSet as $set) {
    $expected = $set[2];
    $actual = $finder->find($set[0], $set[1]);

    if ($actual === $expected) {
        echo '.';
    } else {
        echo 'E';
        $errors[] = 'Expect: ' . implode(' -> ', $expected) . PHP_EOL. 'Actual: ' . implode(' -> ', $actual);
    }
}

echo PHP_EOL . PHP_EOL;

if (empty($errors)) {
    echo 'Success' . PHP_EOL;
} else {
    echo 'Failed' . PHP_EOL . implode(PHP_EOL . PHP_EOL, $errors) . PHP_EOL;
}
